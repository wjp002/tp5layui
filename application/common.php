<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

//一维数组无限级分类
function erwei($cate, $pid = 0)
{
    $arr = array();
    foreach ($cate as $v) {
        if ($v['pid'] == $pid) {
            if ($v['ismenu'] == 1) {
                $v['child'] = erwei($cate, $v['id']);
                $arr[] = $v;
            }
        }
    }
    return $arr;
}

function tree($table, $p_id = 0)
{
    $tree = array();
    foreach ($table as $row) {
        if ($row['pid'] == $p_id) {
            $row['checkArr'] = "0";
            $tmp = tree($table, $row['id']);
            if ($tmp) {
                $row['children'] = $tmp;
            } else {
                $row['leaf'] = true;
            }
            $tree[] = $row;
        }
    }
    Return $tree;
}