<?php

namespace app\admin\controller;

use think\Controller;
use think\Db;
use app\admin\model\SysroleModel;
use app\admin\model\SysmenuModel;

class WidgetController extends Controller
{
    public function widgetUserMenuList($userid = 0)
    {
        //$usermenulist = Db::name("sysmenu")->alias("m ")->join("sysrole_menu rm ", ["rm.menu_id=m.id"])
        //    ->join("sysrole_user ru", [" ru.role_id=rm.role_id",])
        //    ->where("ru.user_id='$userid'")->select();
        $usermenulist = Db::query("select DISTINCT sysmenu.* from sysmenu inner join sysrole_menu on sysmenu.id=sysrole_menu.menu_id inner join sysrole_user on sysrole_menu.role_id=sysrole_user.role_id and sysrole_user.user_id=:user_id", ['user_id' => $userid]);
        return $usermenulist;
    }

    public function widgetRoleMenuList($roleid = 0)
    {
        $menulist = Db::name("sysmenu")->alias("m ")->join("sysrole_menu rm ", ["rm.menu_id=m.id"])
            ->where("rm.role_id='$roleid'")->field('m.id,m.title,m.name')->select();
        return $menulist;
    }

    public function widgetRoleList()
    {
        $sysrolemodel = new SysroleModel();
        return $sysrolemodel->where("status", 1)->select();
    }

    public function widgetUserRoleList($userid = 0)
    {

        $sysrolemodel = new SysroleModel();
        return $sysrolemodel->join("sysrole_user ru ", ["sysrole.id=ru.role_id"])->where("ru.user_id=" . $userid)->select();
    }

    public function widgetMenu()
    {
        $sysmenu = new SysmenuModel();
        $sysmenulist = $sysmenu->select();
        $sysmenulist = tree($sysmenulist);
        $json = [
            "data"   => $sysmenulist,
            "status" => [
                "code"    => 200,
                "message" => "成功",
            ],
        ];
        return json($json);
    }
}